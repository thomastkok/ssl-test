import re
import os

import docker
from flask import Flask
from yattag import Doc

app = Flask(__name__)


def get_apps():
    apps = []
    client = docker.from_env()
    for c in client.containers.list():
        for env in c.attrs['Config']['Env']:
            if 'VIRTUAL_HOST' in env:
                match = re.match('(.*?)=~\\^([^\\\\]+)\\.*', env)
                if match:
                    apps.append(match.group(2))
    return apps


def to_html(apps):
    doc, tag, text = Doc().tagtext()
    with tag('html'):
        with tag('body'):
            with tag('h1'):
                text('Running apps')
            with tag('ul'):
                for a in apps:
                    with tag('li'):
                        with tag('a', href=f"http://{a}.{os.environ['HOST_IP']}.xip.io"):
                            text(a.replace('auth-', ''))
    return doc.getvalue()


@app.route("/")
def index():
    apps = get_apps()
    return to_html(apps)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
