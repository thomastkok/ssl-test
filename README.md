## Dockerfile.pipenv
Contains a template for building apps whose requirements are governed by Pipenv. Image is built in Gitlab CI, or manually with
```
docker build -t dupont/pipenv -f Dockerfile.pipenv .
```
See https://docs.docker.com/engine/reference/builder/#onbuild

## Reminders for deploying a new app
- Set the `VIRTUAL_HOST=${~^<app_name>\..*\.xip\.io}` env in the
  container to make it available in the index. Make sure you pick a
  name that's not already in use. Xip.io is used to avoid having to
  set starred DNS rules. Prefix your app name by 'auth-' to make nginx
  use LDAP authentication for your app.
- Expose only one port, or set the `VIRTUAL_PORT=*app_port*` env in the container.
- Make sure to attach the container to the `deployment_default` network
- If using pipenv, you might want to use the `dupont/pipenv` image (build from Dockerfile.pipenv)
- If your app delivers services on multiple ports, you should split it up into multiple apps
- Make sure to set any envs required at deploy time in the Gitlab CI Variables overview
- Make sure the supplied `SSH_PRIVATE_KEY` variable is RSA and in PEM-format (`ssh-keygen -m PEM`)
- Remember to update the lock (`pipenv lock`) if you make changes to your custom packages.

## On SSH keypairs
Keys must be supplied in PEM-format.
```
ssh-keygen -m PEM
```

## Setting up https

Generate certificate named according to CERT_NAME

```
cd nginx-proxy && mkdir certificates
openssl genrsa -out certificates/localhost.key 2048
openssl req -new -key certificates/localhost.key -out certificates/localhost.csr -subj '/CN=localhost'
openssl x509 -req -days 365 -in certificates/localhost.csr -signkey certificates/localhost.key -out certificates/localhost.crt
```

## Install gitlab runner

See https://docs.gitlab.com/runner/install/linux-manually.html

Leiden network is bad. Set

```
ExecStart=/usr/bin/dockerd --max-concurrent-downloads 1 -H fd:// --containerd=/run/containerd/containerd.sock
```

in `/usr/lib/systemd/system/docker.service` to reduce risk of TLS handshake timeout.


